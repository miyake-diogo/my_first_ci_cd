# Project to learn about TDD

Reference Video: https://www.youtube.com/watch?v=eAPmXQ0dC7Q

Rference Repo: https://github.com/wesdoyle/flask-ner

To run test: 
Enter in test folder and write: 
*python -m unittest filename.class_name.method_name*

Example:
```
python -m unittest test_ner_client.TestNerClient.test_get_ents_returns_dictionary_given_empty_string 
```